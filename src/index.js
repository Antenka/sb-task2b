import express from 'express';

const app = express();


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

function upFLet(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

app.get('/task2B', (req, res) => {
  const fName = req.query.fullname.replace(/\s\s+/g, ' ').trim().toLowerCase().split(' ');

  const re = new RegExp('([0-9_/@#$%&])');
  const hasNumbers = req.query.fullname.match(re) !== null;
  if (fName.length > 3 || fName.length < 1 || fName[0] === '' || hasNumbers) {
    return res.send('Invalid fullname');
  }

  switch (fName.length) {
    case 1:
      return res.send(upFLet(fName[0]));
    case 2:
      return res.send(upFLet(fName[1]) + ' ' + upFLet(fName[0]).charAt(0) + '.');
    case 3:
      return res.send(upFLet(fName[2]) + ' ' + upFLet(fName[0]).charAt(0) + '. ' + upFLet(fName[1]).charAt(0) + '.');
    default:
      return res.send('Invalid fullname');
  }
});

app.post('/task2A', (req, res) => {
  const a = +req.query.a || 0;
  const b = +req.query.b || 0;

  res.send('' + (a + b));
});


app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
